/* ------------------ BEGIN FUNCTION SECTION */

// Generate a universal "Student View" button on 
var universalStudentView = function() {
    var isTeacher, isCourse, courseId, templateButton, sutdentViewVisible, studentViewURL, getPath;
    
    // Determine if the user is actually a teacher
    if (ENV['current_user_roles'].includes('teacher') && $('.ic-app-course-menu.list-view nav #section-tabs li.section a[title="Settings"]').is(":visible")) {
        isTeacher = true;
    } else {
        isTeacher = false;
    }

    // Get the current Course ID and path based on the url of the course
    isCourse = window.location.href.indexOf("/courses/") > -1;
    getPath = window.location.pathname;
    
    // Determine if the template button is currently visible
    templateButton = $('.btn.button-sidebar-wide.element_toggler.choose_home_page_link').is(':visible');
    
    // Determine if the user is currently in student view
    studentViewVisible = $('.ic-alert-masquerade-student-view').is(':visible');

    // Validate rendering the universal button based on the variables

    if(isTeacher && isCourse && studentViewVisible == false) { // If the user is truly a teacher for this course/student view is not already enabled, render the button
        courseId = getPath.split('courses/').pop().split('/')[0]
        studentViewURL = "/courses/" + courseId + "/student_view";
        $('.ic-app-nav-toggle-and-crumbs.no-print').append('<a class="btn button-sidebar-wide quick-access" href="' + studentViewURL + '" rel="nofollow" data-method="post"><i class="icon-student-view" role="presentation"></i> Launch Student View</a>')
    } else { // If the user is not a teacher or student view is already enabled
        $('.ic-app-nav-toggle-and-crumbs.no-print').remove('.btn.button-sidebar-wide.quick-access')
    }
}

/* ------------------ END FUNCTION SECTION */

/* ------------------ RUN ON PAGE LOAD */

$(document).ready(function () {
	universalStudentView()
})